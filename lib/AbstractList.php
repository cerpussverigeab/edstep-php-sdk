<?php

namespace Edstep;

abstract class AbstractList implements \IteratorAggregate, \ArrayAccess {

  /**
   * @var Client $client The client that is used to make requests.
   */
  protected $client;

  /**
   * @var string $path The path used for GETing the data.
   */
  protected $path;

  /**
   * @var array $data Contains fetched or set data.
   */
  protected $data;

  /**
   * @var array $items Contains items for the iterator.
   */
  protected $items;

  /**
   * @param Client $client The client to use for requests.
   * @param string $path The path used for GETing the data.
   */
  public function __construct(Client $client, $path) {
    $this->client = $client;
    $this->path = $path;
  }

  /**
   * @return string The path used for GETing the data.
   */
  public function getPath() {
    return $this->path;
  }

  /**
   * Requests the data for this object via the client object. If no `path` is set it does nothing.
   * @return $this
   */
  public function fetch() {
    $path = $this->getPath();
    if(!$path) {
      return;
    }

    /**
     * Remove duplicate courses. Workaround until  https://jira.cerpus.com/browse/CCC-2345 is resolved.
     */
    $data = $data_tmp = $this->client->get($path);
    $fetched_ids = array();
    foreach($data_tmp as $index => $fetched_data) {
      if(!in_array($fetched_data['id'], $fetched_ids)) {
       $fetched_ids[] = $fetched_data['id'];
      }
      else {
        unset($data[$index]);
      }
    }

    $this->data = $data;
    return $this;
  }

  /*
   * {@inheritDoc}
   */
  public function getIterator() {
    return new \ArrayIterator($this->toArray());
  }

  /**
   * Converts this object into an array by first fetching the referenced data and then creating the items.
   * @return array An array containing the items of this list
   */
  public function toArray() {
    if(!isset($this->items)) {
      $this->fetch();
      $this->items = array_map(array($this, 'createItem'), $this->data ?: []);
    }
    return $this->items;
  }

  /*
   * {@inheritDoc}
   */
  public function offsetSet($offset, $value) {
    throw new \Exception();
  }

  /*
   * {@inheritDoc}
   */
  public function offsetExists($offset) {
    return isset($this->toArray()[$offset]);
  }

  /*
   * {@inheritDoc}
   */
  public function offsetUnset($offset) {
    throw new \Exception();
  }

  /*
   * {@inheritDoc}
   */
  public function offsetGet($offset) {
    return isset($this->toArray()[$offset]) ? $this->toArray()[$offset] : null;
  }

  /**
   * Creates an item of the right class for this list.
   * @param mixed[] $data Data for the new object
   * @return AbstractObject An object with the passed data
   */
  abstract public function createItem(array $data);

}
