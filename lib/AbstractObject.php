<?php

namespace Edstep;

abstract class AbstractObject {

  /**
   * @var Client $client The client that is used to make requests.
   */
  protected $client;

  /**
   * @var array $data Contains fetched or set data.
   */
  protected $data;

  /**
   * @param Client $client The client to use for requests.
   * @param mixed[] $data Initial data for the object.
   */
  public function __construct(Client $client, $data = []) {
    $this->client = $client;
    $this->data = $data;
  }

  /**
   * Handles access of inaccessible properties. For inaccessible property `foo` it will first look for a method called `getFoo`. If that doesn’t exist it will look in the `data` property for the `foo` key. If that doesn’t exist it will perform a fetch via the client. If the data still isn’t found it will return `NULL`.
   */
  public function __get($name) {
    $func = 'get' . ucfirst($name);
    if(method_exists($this, $func)) {
      return call_user_func([$this, $func]);
    }
    if(!array_key_exists($name, $this->data)) {
      $this->data[$name] = NULL;
      $this->fetch();
    }
    return $this->data[$name];
  }

  /**
   * Unsettable properties will be passed to the `data` property.
   */
  public function __set($name, $value) {
    $this->data[$name] = $value;
  }

  /**
   * @return string Base path for GET endpoint without trailing or leading slash.
   */
  abstract public function getBasePath();

  /**
   * @return string Path for GET endpoint without trailing or leading slash.
   */
  public function getPath() {
    return $this->getBasePath() . '/' . $this->id;
  }

  /**
   * Fetches data via the API and stores it.
   * @return $this
   */
  public function fetch() {
    $path = $this->getPath();
    if(!$path) {
      return;
    }
    $data = $this->client->get($path);

    $this->data = array_merge($this->data, $data);
    return $this;
  }

  /**
   * @return array The stored data for this object.
   */
  public function toArray() {
    return $this->data;
  }

}
