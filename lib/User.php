<?php

namespace Edstep;

use Edstep\Exception\InvalidParamException;

class User extends AbstractObject {

  /**
   * @param Client $client The client to use for requests.
   * @param mixed[] $data Initial data for the object.
   */
  public function __construct(Client $client, $data = []) {
    parent::__construct($client, $data);
    $this->usePublic = FALSE;
  }

  /**
   * @return string Base path for GET endpoint without trailing or leading slash.
   */
  public function getBasePath() {
    return 'users';
  }

  /**
   * @return string Path for GET endpoint without trailing or leading slash.
   */
  public function getPath() {
    if($this->id != 'me') {
      throw new InvalidParamException('Fetching users with arbitrary IDs is not supported. Only "me" is valid ID for User objects.');
    }
    return $this->getBasePath();
  }

  /**
   * Returns a list of courses for the current user, filtered by one of the valid filters.
   * @param string $filter One of "owned", "ongoing" or "completed".
   * @return CourseList A list of courses as defined by the provided filter
   */
  function courses($filter) {
    switch($filter) {
      case 'owned':
      case 'ongoing':
      case 'completed':
        return new CourseList($this->client, $this->getPath() . '/' . $filter . 'Courses');
        break;
    }
    throw new \InvalidArgumentException("'$filter' is not a valid filter for courses");
  }

  /**
   * Handles access of inaccessible properties. If the property looks like `xxxCourses`, `User::courses($filter)` will be called. {@see AbstractObject::__get} for other cases.
   */
  function __get($key) {
    if(preg_match('/^(\w+)Courses$/', $key, $matches)) {
      return $this->courses($matches[1]);
    }
    return parent::__get($key);
  }

}
