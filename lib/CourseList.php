<?php

namespace Edstep;

use Edstep\Client;

class CourseList extends AbstractList {

  /*
   * {@inheritDoc}
   */
  public function createItem(array $data) {
    return new Course($this->client, $data);
  }

}
