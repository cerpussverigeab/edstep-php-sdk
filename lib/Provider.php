<?php

namespace Edstep;

use \League\OAuth2\Client\Provider\GenericProvider;

class Provider extends GenericProvider {

  /**
   * Decodes a serialized state
   * @param array $state The state to serialize
   * @return mixed The encoded state
   */
  static function encodeState($state) {
    return strtr(base64_encode(json_encode($state)), '+/', '-_');
  }

  /**
   * Decodes a serialized state
   * @param string $string The string to decode. Should be base64 encoded JSON with `+` replaced with `-` and `/` replaced with `_`
   * @return array|false `FALSE` if an error occurred or an array otherwise.
   */
  static function decodeState($string) {
    return json_decode(base64_decode(strtr($string, '-_', '+/')), TRUE);
  }

  /**
   * Checks if the state in the request URI matches the expected state.
   * @param array $state The expected state
   * @return boolean `TRUE` if it matches or `FALSE` otherwise
   */
  public function checkState(array $state) {
    return (!empty($_GET['state']) && ($_GET['state'] === static::encodeState($state)));
  }

  /*
   * {@inheritDoc}
   */
  public function getAuthorizationUrl(array $state = []) {
    $state['csrf'] = bin2hex(random_bytes(16));
    return parent::getAuthorizationUrl([
      'state' => static::encodeState($state),
    ]);
  }

  /*
   * {@inheritDoc}
   */
  public function getState() {
    $state = parent::getState();
    if(isset($state)) {
      return static::decodeState($state);
    }
    return $state;
  }

  /*
   * {@inheritDoc}
   */
  protected function getAuthorizationParameters(array $options) {
    $options = parent::getAuthorizationParameters($options);
    if(!empty($this->useidp)) {
      $options['useidp'] = $this->useidp;
    }
    if(!empty($this->requirements)) {
      $options['requirements'] = $this->requirements;
    }
    return $options;
  }

  /*
   * {@inheritDoc}
   */
  protected function getConfigurableOptions() {
    return array_merge(parent::getConfigurableOptions(), [
      'useidp',
      'requirements',
    ]);
  }

}
