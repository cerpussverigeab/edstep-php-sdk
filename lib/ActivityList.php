<?php

namespace Edstep;

class ActivityList extends AbstractList {

  /**
   * @var Section $section The section that the activities in this list belongs to.
   */
  public $section;

  /*
   * {@inheritDoc}
   */
  public function createItem(array $data) {
    $activity = new Activity($this->client, $data);
    $activity->section = $this->section;
    return $activity;
  }

}
