<?php

namespace Edstep;

class Course extends AbstractObject {

  /**
   * @var boolean $usePublic whether to use /public request or not.
   */
  protected $usePublic;

  /**
   * @param Client $client The client to use for requests.
   * @param mixed[] $data Initial data for the object.
   */
  public function __construct(Client $client, $data = []) {
    parent::__construct($client, $data);
    $this->usePublic = FALSE;
  }

  /**
   * @return string Base path for GET endpoint without trailing or leading slash.
   */
  public function getBasePath() {
    return 'courses';
  }

  /*
   * {@inheritDoc}
   */
  public function getPath() {
    if($this->usePublic) {
      return $this->getBasePath() . '/' . $this->id . '/public';
    }
    return $this->getBasePath() . '/' . $this->id;
  }

  /**
   * @return $this
   */
  public function enroll() {
    $this->client->post($this->getBasePath() . '/' . $this->id . '/enroll');
    return $this;
  }

  /**
   * @param boolean $value The value to set. Defaults to `TRUE`.
   * @return $this
   */
  public function usePublic($value = TRUE) {
    $this->usePublic = $value;
    return $this;
  }

  /**
   * @return SectionList An EdStep section list object
  */
  public function getSections() {
    $section_list = new SectionList($this->client, $this->getBasePath() . '/' . $this->id . '/modules');
    $section_list->course = $this;
    return $section_list;
  }

  /**
   * Constructs a new section object that can be used to fetch data about a specific section.
   * @param string $id The section ID
   * @return Section An EdStep section object
   */
  public function section($id) {
    $section = new Section($this->client);
    $section->id = $id;
    $section->course = $this;
    return $section;
  }

  public function completed() {
    return $this->client->post($this->getBasePath() . '/' . $this->id . '/completed')['completed'];
  }

}
