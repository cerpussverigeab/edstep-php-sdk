<?php

namespace Edstep;

use Edstep\Exception\RefreshAccessTokenException;
use GuzzleHttp\Client as Guzzle;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use League\OAuth2\Client\Token\AccessToken;

/**
 * The client is responsible for all HTTP requests, authentication and constructing lists and objects representing EdStep data.
 * @property AccessToken $accessToken {@see Client::getAccessToken()} and {@see Client::setAccessToken()}
 * @property-read CourseList $courses {@see Client::getCourses()}
 * @property-read User $currentUser {@see Client::getCurrentUser()}
 * @property-read Provider $provider {@see Client::getProvider()}
 */
class Client {

  /**
   * @var Client $guzzle The Guzzle client used for requests.
   */
  protected $guzzle;

  /**
   * @var array $settings Settings for the client.
   */
  protected $settings;

  /**
   * @var Provider $provider The OAuth2 provider.
   */
  protected $provider;

  /**
   * @var AccessToken $accessToken The OAuth2 access token.
   */
  protected $accessToken;

  /**
   * @var array $cache Holds cached GET requests
   */
  protected $cache;

  /**
   * @var array $requestExceptions Holds cached GET request exceptions
   */
  protected $requestExceptions;

  /**
   * Constructs a new EdStep client
   * @param array $options Settings for the client.
   */
  public function __construct(array $options = array()) {
    $this->settings = $options + array(
      'host' => 'https://edstep.com',
      'version' => 'v1',
      'auth_host' => 'https://auth.cerpus-course.com',
      'auth_client_id' => 'xxx',
      'auth_client_secret' => 'xxx',
      'auth_redirect_uri' => '',
    );

    $this->provider = new Provider([
      'clientId' => $this->settings['auth_client_id'],
      'clientSecret' => $this->settings['auth_client_secret'],
      'redirectUri' => $this->settings['auth_redirect_uri'],
      'urlAuthorize' => $this->settings['auth_host'] . '/oauth/authorize',
      'urlAccessToken' => $this->settings['auth_host'] . '/oauth/token',
      'urlResourceOwnerDetails' => $this->settings['auth_host'] . '/v1/identity',
      'scope' => 'read',
      'useidp' => $this->settings['auth_useidp'],
      'requirements' => $this->settings['auth_requirements'],
    ]);

    $this->cache = array();
  }

  /**
   * Handles access of inaccessible properties. For inaccessible property `foo` it will look for a method called `getFoo`. If that doesn’t exist an error will be thrown by `call_user_func`.
   * @param string $name The name of the property
   */
  public function __get($name) {
    $func = 'get' . ucfirst($name);
    return call_user_func([$this, $func]);
  }

  /**
   * Handles access of inaccessible properties. For inaccessible property `foo` it will look for a method called `setFoo`. If that doesn’t exist an error will be thrown by `call_user_func`.
   * @param string $name The name of the property
   * @param mixed $value The new value for the property
   */
  public function __set($name, $value) {
    $func = 'set' . ucfirst($name);
    return call_user_func([$this, $func], $value);
  }

  /**
   * Returns the OAuth2 provider for this EdStep client.
   * @return Provider The OAuth2 provider.
   */
  public function getProvider() {
    return $this->provider;
  }

  /**
   * Returns the Guzzle client for this EdStep client.
   * @return Client The cached Guzzle client.
   */
  protected function guzzle() {
    if(!isset($this->guzzle)) {
      $this->guzzle = new Guzzle(array(
        'base_uri' => $this->settings['host'] . '/api/' . $this->settings['version'] . '/',
      ));
    }
    return $this->guzzle;
  }

  /**
   * Makes a GET request to the specified url.
   * @param string $url The URL of the request
   * @return array|null The response body decoded into an array
   */
  public function get($url) {
    $access_token = $this->getAccessToken();
    if($access_token) {
      $request = $this->provider->getAuthenticatedRequest(
        'GET',
        $url,
        $access_token
      );
    } else {
      $request = new Request(
        'GET',
        $url
      );
    }
    $target = $request->getRequestTarget();
    if(isset($this->requestExceptions[$target])) {
      throw $this->requestExceptions[$target];
      return $this->cache[$target];
    }
    if(!isset($this->cache[$target])) {
      try {
        $response = $this->guzzle()->send($request);
        $this->cache[$target] = json_decode($response->getBody(), TRUE);
      } catch(RequestException $e) {
        $this->cache[$target] = array();
        $this->requestExceptions[$target] = $e;
        throw $e;
      }
    }
    return $this->cache[$target];
  }

  /**
   * Makes a POST request to the specified url.
   * @param string $url The URL of the request
   * @return array|null The response body decoded into an array
   */
  public function post($url) {
    // TODO: Allow request body
    $access_token = $this->getAccessToken();
    if($access_token) {
      $request = $this->provider->getAuthenticatedRequest(
        'POST',
        $url,
        $access_token
      );
      $response = $this->guzzle()->send($request);
    }
    else {
      $response = $this->guzzle()->post($url);
    }
    return json_decode($response->getBody(), TRUE);
  }

  /**
   * Clears all cached GET request responses and exceptions.
   * @return $this
   */
  public function clearCache() {
    $this->cache = [];
    $this->requestExceptions = [];
    return $this;
  }

  /**
   * Constructs a new course object that can be used to fetch data about a specific course.
   * @param string $id The course ID
   * @return Course An EdStep course object
   */
  public function course($id) {
    $course = new Course($this);
    $course->id = $id;
    return $course;
  }

  /**
   * Alias for method `getCourses()` and magic property `courses`.
   * @see Client::getCourses()
   */
  public function courses() {
    return $this->getCourses();
  }

  /**
   * Returns all courses that are associated with the current user.
   * @return CourseList A list of all courses associated witht he current user
   */
  public function getCourses() {
    return new CourseList($this, 'courses');
  }

  /**
   * Requests an access token from the Auth service and stores it in `$this->accessToken`.
   * @return $this
   */
  public function fetchAccessToken() {
    $this->accessToken = $this->provider->getAccessToken('authorization_code', [
      'code' => $_GET['code']
    ]);
    return $this;
  }

  /**
   * Returns the access token object that has been fetched or set manually.
   * @return AccessToken An access token object
   */
  public function getAccessToken() {
    return $this->accessToken;
  }

  /**
   * Manually sets the access token for this client.
   * @param AccessToken $token An access token object
   * @return $this
   */
  public function setAccessToken(AccessToken $token) {
    $this->accessToken = $token;
    return $this;
  }

  /**
   * Tries to refresh the access token by using the refresh token stored on it. Throws a `RefreshAccessTokenException` if no access token is set or if the request fails.
   * @param AccessToken $token An access token object
   * @throws RefreshAccessTokenException
   * @return $this
   */
  public function refreshAccessToken() {
    if(!isset($this->accessToken)) {
      throw new RefreshAccessTokenException('No refresh token specified');
    }
    try {
      $this->accessToken = $this->provider->getAccessToken('refresh_token', [
        'refresh_token' => $this->accessToken->getRefreshToken(),
      ]);
    } catch(IdentityProviderException $e) {
      throw new RefreshAccessTokenException('Could not refresh access token', 0, $e);
    }
    return $this;
  }

  /**
   * Returns a User object with `id = 'me'` which can fetch data for the current user as determined by the access token.
   * @return User The current user as determined by the access token
   */
  public function getCurrentUser() {
    return new User($this, [
      'id' => 'me',
    ]);
  }

}
