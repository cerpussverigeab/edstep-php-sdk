<?php

namespace Edstep;

class SectionList extends AbstractList {

  /**
   * @var SectionList $course The course this sectionlist belongs to.
   */
  public $course;

  public function createItem(array $data) {
    $section = new Section($this->client, $data);
    $section->course = $this->course;
    return $section;
  }

}
