<?php

namespace Edstep;

class Section extends AbstractObject {

  public $course;

  /**
   * @return string Base path for GET endpoint without trailing or leading slash.
   */
  public function getBasePath() {
    return 'courses';
  }

  /*
   * {@inheritDoc}
   */
  public function getPath() {
    return $this->getBasePath() . '/' . $this->course->id . '/modules/' . $this->id;
  }

  public function getActivities() {
    $activity_list = new ActivityList($this->client, $this->getBasePath() . '/' . $this->course->id . '/modules/' . $this->id . '/activities');
    $activity_list->section = $this;
    return $activity_list;
  }

  /**
   * Constructs a new course object that can be used to fetch data about a specific course.
   * @param string $id The course ID
   * @return Course An EdStep course object
   */
  public function activity($id) {
    $activity = new Activity($this->client);
    $activity->id = $id;
    $activity->section = $this;
    return $activity;
  }

}
