<?php

namespace Edstep;

class Activity extends AbstractObject {

  /**
   * @var Section $section The section that this activity belongs to.
   */
  public $section;

  /*
   * {@inheritDoc}
   */
  public function getBasePath() {
    return 'courses';
  }

  /*
   * {@inheritDoc}
   */
  public function getPath() {
    return $this->getBasePath() . '/' . $this->section->course->id . '/modules/' . $this->section->id . '/activity/' . $this->id;
  }

}
