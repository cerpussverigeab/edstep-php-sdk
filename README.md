# EdStep SDK for PHP

This package is a convenience wrapper around the REST API for [edstep.com](https://edstep.com/). It handles HTTP requests and OAuth authentication for you, and you don’t have to know about the REST endpoints to use it.

Start by instantiating a new `Edstep\Client` with your settings:

```php
<?php
$settings = [
  'auth_client_id' => 'YOUR_AUTH_CLIENT_ID',
  'auth_client_secret' => 'YOUR_AUTH_CLIENT_SECRET',
  'auth_redirect_uri' => 'YOUR_AUTH_REDIRECT_URI',
];

$client = new Edstep\Client($settings);
?>
```

Then fetch data like this:

```php
<?php
$course = $client->course(42);
echo $course->title;
echo $course->description;

$my_courses = $client->currentUser->courses;
foreach($my_courses as $course) {
  echo $course->title;
  echo $course->description;
}

$sections = $client->course(42)->sections;
foreach($sections as $section) {
  echo $section->title;
}

$activity = $client->course(42)->section(11)->activity(22);
echo $activity->title;
?>
```
